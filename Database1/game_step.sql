﻿CREATE TABLE [dbo].[game_step]
(
	[id] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [node_from] VARCHAR(4) NOT NULL, 
    [node_to] VARCHAR(4) NOT NULL, 
    [player] SMALLINT NOT NULL, 
    [move_id] BIGINT NOT NULL, 
    CONSTRAINT [FK_game_step_move] FOREIGN KEY ([move_id]) REFERENCES [move]([id])
)
