﻿CREATE TABLE [dbo].[possible_move]
(
	[id] BIGINT NOT NULL PRIMARY KEY IDENTITY, 
    [move_id] BIGINT NULL, 
    [game_step_id] BIGINT NULL, 
    [info] VARCHAR(50) NULL , 
    CONSTRAINT [FK_possible_move_game_step] FOREIGN KEY ([move_id]) REFERENCES [move]([id]), 
    CONSTRAINT [FK_possible_move_ToTable] FOREIGN KEY ([game_step_id]) REFERENCES [game_step]([id])
)
