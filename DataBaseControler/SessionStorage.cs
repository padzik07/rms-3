﻿using NHibernate;
using NHibernate.Cfg;

namespace DataBaseControler
{
    public class SessionStorage : ISessionStorage
    {
        private Configuration _configuration;
        private ISessionFactory _sessionFactory;
        private ISession _session;

        public Configuration Configuration
        {
            get => _configuration;
            set => _configuration = value;
        }

        public ISessionFactory SessionFactory
        {
            get => _sessionFactory;
            set => _sessionFactory = value;
        }

        public ISession Session
        {
            get => _session;
            set => _session = value;
        }

        public SessionStorage()
        {
            if (_session != null && _session.IsOpen)
            {
                _session.Close();
            }

            if (_sessionFactory != null && !_sessionFactory.IsClosed)
            {
                _sessionFactory.Close();
            }
            _configuration = new Configuration();
            _configuration.Configure("C:\\Users\\krzysztof.padzik\\source\\repos\\RMS2\\DataBaseControler\\hibernate.cfg.xml");
            _configuration.AddFile("C:\\Users\\krzysztof.padzik\\source\\repos\\RMS2\\DataBaseControler\\XMLMappingGameState.xml");
            _configuration.AddFile("C:\\Users\\krzysztof.padzik\\source\\repos\\RMS2\\DataBaseControler\\XMLMappingPossibleMove.xml");
            CreateConfiguration();
        }

        private void CreateConfiguration()
        {
            _sessionFactory = _configuration.BuildSessionFactory();
            _session =  _sessionFactory.OpenSession();
        }

        public void ClearSession()
        {
            if (_session != null && _session.IsOpen)
            {
                _session.Close();
            }

            if (_sessionFactory != null && !_sessionFactory.IsClosed)
            {
                _sessionFactory.Close();
            }
            RestartSession();
        }

        private void RestartSession()
        {
            CreateConfiguration();
        }
    }
}
