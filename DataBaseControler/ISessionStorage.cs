﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler
{
    public interface ISessionStorage
    {
        void ClearSession();

        Configuration Configuration {  get; set; }

        ISessionFactory SessionFactory { get; set; }

        ISession Session { get; set; }
    }
}
