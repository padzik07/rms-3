﻿using Common.Shared.BaseDto;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.Service
{
    public class CreateService<T> : ICreateService<T> where T : BaseDto
    {
        private readonly ISessionStorage _sessionStorage;

        public CreateService(ISessionStorage sessionStorage)
        {
            _sessionStorage = sessionStorage;
        }

        public T Execute(T Dto)
        {
            _sessionStorage.Session.Transaction.Begin();
            var ret = _sessionStorage.Session.Save(Dto);
            _sessionStorage.Session.Transaction.Commit();
            return Dto;
        }
    }
}
