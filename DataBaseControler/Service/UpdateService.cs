﻿using Common.Shared.BaseDto;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.Service
{
    public class UpdateService<T> : IUpdateService<T> where T : BaseDto
    {
        private readonly ISessionStorage _sessionStorage;

        public UpdateService(ISessionStorage sessionStorage)
        {
            _sessionStorage = sessionStorage;
        }
        public T Execute(T Dto)
        {
            _sessionStorage.Session.Transaction.Begin();
            _sessionStorage.Session.Update(Dto);
            _sessionStorage.Session.Transaction.Commit();
            return Dto;
        }
    }
}
