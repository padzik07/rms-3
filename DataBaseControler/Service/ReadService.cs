﻿using Common.Shared.BaseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.Service
{
    public class ReadService<T> : IReadService<T> where T : BaseDto
    {
        private readonly ISessionStorage _sessionStorage;

        public ReadService(ISessionStorage sessionStorage)
        {
            _sessionStorage = sessionStorage;
        }

        public T Execute()
        {
            var cryteria = _sessionStorage.Session.CreateCriteria<T>();
            return cryteria.FutureValue<T>().Value;
        }

        public T Execute(long id)
        {
            var cryteria = _sessionStorage.Session.CreateCriteria<T>();
            return cryteria.List<T>().First(x => x.Id == id);
        }
    }
}
