﻿using Common.Shared.BaseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.Service
{
    public interface IBaseCommand<T> where T : BaseDto
    {
        T Execute(T Dto);
    }
}
