﻿using Common.Shared.BaseDto;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.Service
{
    public class DeleteService<T> : IDeleteService<T> where T : BaseDto
    {
        private readonly ISessionStorage _sessionStorage;

        public DeleteService(ISessionStorage sessionStorage)
        {
            _sessionStorage = sessionStorage;
        }
        public T Execute(T Dto)
        {
            _sessionStorage.Session.Transaction.Begin();
            _sessionStorage.Session.Delete(Dto);
            _sessionStorage.Session.Transaction.Commit();
            return null;
        }
    }
}
