﻿using Common.Shared.BaseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.DTO
{
    public class GameStepDto : BaseDto
    {
        public virtual string GameState { get; set; }
        public virtual int? Player { get; set; }
        public virtual int? Winner { get; set; }
    }
}
