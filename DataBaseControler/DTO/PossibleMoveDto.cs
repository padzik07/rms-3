﻿using Common.Shared.BaseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.DTO
{
    public class PossibleMoveDto : BaseDto
    {
        public virtual long? PrivGameStepId { get; set; }
        public virtual long? NextGameStepId { get; set; }
        public virtual string Info { get; set; }
    }
}
