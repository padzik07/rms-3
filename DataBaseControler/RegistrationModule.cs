﻿using Autofac;
using Common.Shared.BaseDto;
using DataBaseControler.CrudRepo;
using DataBaseControler.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterGeneric(typeof(CreateService<>))
                .AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(UpdateService<>))
                .AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(DeleteService<>))
                .AsImplementedInterfaces();
            
            builder.RegisterGeneric(typeof(ReadService<>))
                .AsImplementedInterfaces();

            builder.RegisterGeneric(typeof(CrudRepository<>))
                .AsImplementedInterfaces();

            builder.RegisterType<SessionStorage>()
                .AsImplementedInterfaces().SingleInstance();

        }
    }
}
