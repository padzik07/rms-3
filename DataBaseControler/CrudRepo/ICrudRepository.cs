﻿using Common.Shared.BaseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.CrudRepo
{
    public interface ICrudRepository<T> where T: BaseDto
    {
        T Create(T Dto);
        T Read();
        T Update(T Dto);
        T Delete(T Dto);
        T Read(long id);
    }
}
