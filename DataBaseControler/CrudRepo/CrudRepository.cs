﻿using Common.Shared.BaseDto;
using DataBaseControler.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseControler.CrudRepo
{
    public class CrudRepository<T> : ICrudRepository<T> where T : BaseDto
    {
        private readonly ISessionStorage _sessionStorage;
        private readonly ICreateService<T> _createService;
        private readonly IUpdateService<T> _updateService;
        private readonly IDeleteService<T> _deleteService;
        private readonly IReadService<T> _readService;

        public CrudRepository(
            ICreateService<T> createService,
            IUpdateService<T> updateService,
            IDeleteService<T> deleteService,
            IReadService<T> readService,
            ISessionStorage sessionStorage)
        {
            _createService = createService;
            _updateService = updateService;
            _deleteService = deleteService;
            _readService = readService;
            _sessionStorage =  sessionStorage;
        }

        public T Create(T Dto)
        {
            _sessionStorage.ClearSession();
            return  _createService.Execute(Dto);
        }

        public T Delete(T Dto)
        {
            _sessionStorage.ClearSession();
            return _deleteService.Execute(Dto);
        }

        public T Read()
        {
            _sessionStorage.ClearSession();
            return _readService.Execute();
        }

        public T Read(long id)
        {
            _sessionStorage.ClearSession();
            return _readService.Execute(id);
        }

        public T Update(T Dto)
        {
            _sessionStorage.ClearSession();
            return _updateService.Execute(Dto);
        }
    }
}
