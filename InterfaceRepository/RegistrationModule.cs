﻿using Autofac;
using InterfaceRepository.Common.Extra.Logger;
using InterfaceRepository.Handlers;
using InterfaceRepository.MoveMaker;
using InterfaceRepository.RMSMain;
using InterfaceRepository.Writer;

namespace InterfaceRepository.Exectution.Registration
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Logger>()
                .As<ILogger>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TreeCheckNaive>()
                .As<ITreeCheck>();
        
            builder.RegisterType<MoveMaker.MoveMaker>()
                .As<IMoveMaker>();

            builder.RegisterType<GameStepHandler>()
                .As<IGameStepHandler>();

            builder.RegisterType<GraphHandler.GraphHandler>()
                .AsImplementedInterfaces();

            builder.RegisterType<InsertWriter>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterType<FileReader.FileReader>()
                .AsImplementedInterfaces();
        }
    }
}
