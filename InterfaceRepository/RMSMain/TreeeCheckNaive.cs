﻿using Common.Shared.DataObjects;
using InterfaceRepository.Common.Extra.Logger;
using InterfaceRepository.Handlers;
using InterfaceRepository.MoveMaker;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterfaceRepository.RMSMain
{
    public class TreeCheckNaive : ITreeCheck
    {
        private readonly ILogger _logger;
        private readonly IMoveMaker _moveMakre;
        private readonly IGameStepHandler _gameStepHandler;
        private string _fileName = "proxyFile.txt";
        private string _filePath = ".";
        private long _id = 0;

        public event EventHandler<string> OnSendMessage;

        public int size;

        public TreeCheckNaive(
            ILogger logger,
            IMoveMaker moveMaker,
            IGameStepHandler gameStepHandler)
        {
            _logger = logger;
            _moveMakre = moveMaker;
            _gameStepHandler = gameStepHandler;
        }

        public void Execution(int size, string startPath)
        {
            _logger.CreateStream(true,$"{_filePath}\\{_fileName}");
           List<string> moveList = _moveMakre.MakeMoveList(size);
            int color = 1;
            string gamePath = startPath;
            if (gamePath != "")
            {
                string[] remove = gamePath.Split(' ');
                foreach (string r in remove)
                {
                    color = 3 - color;
                    moveList.Remove(r);
                }
            }
            
            var game = Game(0, moveList, color, gamePath, size);
            _logger.Close();
            Write("Wygrywa gracz " + game.Winner);
            _gameStepHandler.LogData($"{_filePath}\\{_fileName}", game.Winner.Value);
            _logger.Close();
        }

        public int IsWin(string gameState)
        {
            string[] moveList = gameState.Split(' ');
            List<string> firstPlayerMoves = new List<string>(), secoundPlayerMoves = new List<string>();
            for (int i = 0; i < moveList.Length; i++)
            {
                if (i % 2 == 0)
                    firstPlayerMoves.Add(moveList[i]);
                else
                    secoundPlayerMoves.Add(moveList[i]);
            }

            foreach (string move in firstPlayerMoves)
            {
                string aPosition = move[0].ToString(), bPosition = move[2].ToString(), cPosition;
                foreach (string move2 in firstPlayerMoves)
                {
                    if (move != move2 && aPosition == move2[0].ToString())
                    {
                        cPosition = move2[2].ToString();
                        foreach (string move3 in firstPlayerMoves)
                        {
                            if (move2 != move3 && move3 == $"{bPosition};{cPosition}")
                            {
                                return 2;
                            }
                        }
                    }

                }
            }

            foreach (string move in secoundPlayerMoves)
            {
                string aPosition = move[0].ToString(), bPosition = move[2].ToString(), cPosition;
                foreach (string move2 in secoundPlayerMoves)
                {

                    if (move != move2 && aPosition == move2[0].ToString())
                    {
                        cPosition = move2[2].ToString();
                        foreach (string move3 in secoundPlayerMoves)
                        {
                            if (move2 != move3 && move3 == $"{bPosition};{cPosition}")
                            {
                                return 1;
                            }
                        }
                    }
                }
            }
            return 0;
        }
        
        private GameStep Game(int deapth,  List<string> moveList, int color, string gamePath, int size)
        {
            var me = new GameStep
            {
                Id = _id++,
                GameState = gamePath,
                Winner = IsWin(gamePath),
                Player = color,
                Info = string.Empty
            };
            
            if (me.Winner == 0 && moveList.Count == 0)
            {
                me.Winner = -1;

                if (me.Info.Length != 0)
                {
                    me.Info = me.Info.Substring(0, me.Info.Length - 1);
                }

                // id|winner|gameState|palyer|[child.id,child.id,...]
                Write($"{me.Id}|{me.Winner}|{me.GameState}|{me.Player}|{me.Info}");

                return me;
            }
            
            if (me.Winner == 0)
            {
                int nodeWinner;
                foreach (string move in moveList)
                {
                    string newGamePath = $"{gamePath} {move}";
                    List<string> newMoveList = new List<string>(moveList);
                    newMoveList.Remove(move);
                    var child = Game(deapth + 1, newMoveList, 3 - color, newGamePath, size);
                    nodeWinner = child.Winner.Value;
                    if (child.Winner == -1)
                    {
                        me.Winner = -1;
                        me.Info += $"{child.Id},";
                        break;
                    }

                    if (child.Winner == color)
                    {
                        me.Winner = color;
                        // CIĘIE GRACZA {color}
                        me.Info += $"{child.Id},";
                        break;
                    }
                    else if (child.Winner == 3 - color)
                    {
                        me.Info += $"{child.Id},";
                    }

                    me.Winner = nodeWinner;
                }
            }
            if (me.Info.Length != 0)
            {
                me.Info = me.Info.Substring(0, me.Info.Length - 1);
            }

            // id|winner|gameState|palyer|[child.id,child.id,...]
            Write($"{me.Id}|{me.Winner}|{me.GameState}|{me.Player}|{me.Info}");
            return me;
        }

        private void Write(string message)
        {
            _logger.Write(message);
        }
    }
}
