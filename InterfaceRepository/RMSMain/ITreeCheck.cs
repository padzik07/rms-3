﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.RMSMain
{
    public interface ITreeCheck
    {
        void Execution(int size, string startPath);
        event EventHandler<string> OnSendMessage;
    }
}
