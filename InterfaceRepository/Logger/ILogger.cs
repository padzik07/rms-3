﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceRepository.Common.Extra.Logger
{
    public  interface ILogger
    {
        void Write(string message);
        void WriteToConsole(string message);
        void WriteToFile(string message);
        void CreateStream(bool isWrite , string filePath = null);
        void Close();
    }
}
