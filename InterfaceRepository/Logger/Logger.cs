﻿using System;
using System.IO;
using System.Windows.Forms;

namespace InterfaceRepository.Common.Extra.Logger
{
    class Logger : ILogger
    {
        private static StreamWriter _streamWriter;
        private static StreamReader _streamReader;
        private static bool _isFileWrite = false;
        private static bool _isFileRead = false;
        
        public Logger()
        {
        }
        
        public void CreateStream(bool isWrite ,string filePath = null)
        {
            Close();
            _isFileWrite = isWrite;
            if (filePath != null)
            {
                _streamWriter = new StreamWriter(filePath);
            }
            else
            {
                _isFileWrite = false;
            }
        }

        public void Write(string message)
        {
            WriteToConsole(message);
            WriteToFile(message);
        }

        public void WriteToConsole(string message)
        {
            Console.WriteLine(message);
        }

        public void WriteToFile(string message)
        {
            if (_isFileWrite == false)
            {
                return;
            }

            _streamWriter.Write(message + Environment.NewLine);
        }

        public void Close()
        {
            if (_streamWriter != null )
            {
                _streamWriter.Close();
            }

            _isFileWrite = false;
        }
    }
}
