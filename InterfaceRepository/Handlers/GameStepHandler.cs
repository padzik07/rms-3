﻿using Common.Enum;
using Common.Shared.DataObjects;
using DataBaseControler.CrudRepo;
using DataBaseControler.DTO;
using InterfaceRepository.Common.Extra.Logger;
using InterfaceRepository.FileReader;
using InterfaceRepository.Writer;
using System.Collections.Generic;

namespace InterfaceRepository.Handlers
{
    public class GameStepHandler : IGameStepHandler
    {
        private readonly ILogger _logger;
        private readonly IInsertWriter _gameStepWriter;
        private readonly IInsertWriter _possibleMoveWriter;
        private readonly ICrudRepository<GameStepDto> _crudRepositoryGameStep;
        private readonly ICrudRepository<PossibleMoveDto> _crudRepositoryPossibleMove;
        private readonly IFileReader _fileReader;
        private bool _isReadStreamOpen = false;

        private List<GameStep> _gameStepsList = new List<GameStep>();
        private int id = 0;
        public GameStepHandler(
            ILogger logger,
            ICrudRepository<GameStepDto> crudRepositoryGameStep,
            ICrudRepository<PossibleMoveDto> crudRepositoryPossibleMove,
            IInsertWriter gameStepWriter,
            IInsertWriter possibleMoveWriter,
            IFileReader fileReader
            )
        {
            _logger = logger;
            _crudRepositoryGameStep = crudRepositoryGameStep;
            _crudRepositoryPossibleMove = crudRepositoryPossibleMove;
            _gameStepWriter = gameStepWriter;
            _possibleMoveWriter = possibleMoveWriter;
            _fileReader = fileReader;
        }

        // id|winner|gameState|palyer|[child.id,child.id,...]
        public void LogData(string proxyFile,int winner)
        {
            var possibleMoveId = 0;
            _fileReader.OpenStream(proxyFile);
            _gameStepWriter.OpenStream(".\\..\\Output\\GameStep.txt", TableNameEnum.GameStep);
            _possibleMoveWriter.OpenStream(".\\..\\Output\\PossibleMove.txt", TableNameEnum.PossibleMoves);
            
            while (_fileReader.IsEnd == false)
            {
                var line = _fileReader.Read;
                var gameStepLine = line.Split('|');
                if (gameStepLine[1] == winner.ToString())
                {
                    _logger.Write(line);
                    dynamic gameStepDto = new
                    {
                        Id = long.Parse(gameStepLine[0]),
                        Winner = int.Parse(gameStepLine[1]),
                        GameState = gameStepLine[2],
                        Player = int.Parse(gameStepLine[3])
                    };
                    _gameStepWriter.WriteInsertLine(gameStepDto);
                    foreach (var childStepId in gameStepLine[4].Split(','))
                    {
                        if (string.IsNullOrEmpty(childStepId))
                        {
                            continue;
                        }

                        dynamic possibleMoveDto = new
                        {
                            Id = possibleMoveId++,
                            PrivGameStep = gameStepDto.Id,
                            NextGameStep = int.Parse(childStepId),
                            Info = string.Empty
                        };
                        _possibleMoveWriter.WriteInsertLine(possibleMoveDto);
                    }
                }
            }

            _fileReader.Close();
            _gameStepWriter.Close();
            _possibleMoveWriter.Close();
        }
    }
}
