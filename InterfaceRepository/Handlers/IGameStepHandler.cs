﻿using Common.Shared.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.Handlers
{
    public interface IGameStepHandler
    {
        void LogData(string proxyFile, int winner);
    }
}
