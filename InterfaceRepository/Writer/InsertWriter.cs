﻿using Common.Enum;
using Common.Extensions;
using System.IO;

namespace InterfaceRepository.Writer
{
    public class InsertWriter : IInsertWriter
    {
        private StreamWriter _streamWriter;
        private TableNameEnum _tableType;

        public InsertWriter()
        {
        }

        public void OpenStream(string file, TableNameEnum tableType)
        {
            _tableType = tableType;
            _streamWriter = new StreamWriter(file);
            _streamWriter.WriteLine($"SET IDENTITY_INSERT [dbo].[{_tableType.GetDescription()}] ON");
        }

        public void Close()
        {
            _streamWriter.Flush();
            _streamWriter.Close();
        }

        public void WriteInsertLine(dynamic dto)
        {
            switch (_tableType)
            {
                case TableNameEnum.GameStep:
                    WriteGameStep(dto.Id,dto.GameState, dto.Player,dto.Winner);
                    break;
                case TableNameEnum.PossibleMoves:
                    WritePossibleMove(dto.Id, dto.PrivGameStep, dto.NextGameStep, dto.Info);
                    break;
                default:
                    break;
            }
        }

        private void WriteGameStep(long id, string gameState, int player, int winner)
        {
            _streamWriter.WriteLine($"INSERT INTO [dbo].[game_step] ([id], [game_state], [player], [winner]) VALUES ({id}, N'{gameState}', {player}, {winner})");
        }

        private void WritePossibleMove(long id, long privGameStep, long nextGameStep,string info)
        {
            _streamWriter.WriteLine($"INSERT INTO [dbo].[possible_moves] ([id], [priv_game_step_id], [next_game_step_id], [info]) VALUES ({id}, {privGameStep}, {nextGameStep}, {info})");
        }
    } 

}