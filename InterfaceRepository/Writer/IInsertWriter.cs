﻿using Common.Enum;

namespace InterfaceRepository.Writer
{
    public interface IInsertWriter
    {
        void OpenStream(string file, TableNameEnum tableType);

        void Close();

        void WriteInsertLine(dynamic dto);
    }
}
