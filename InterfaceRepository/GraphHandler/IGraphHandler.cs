﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.GraphHandler
{
    public interface IGraphHandler
    {
        List<List<int>> ConmertToMatrix(string graph,int size);

        bool CheckTwoMatrises(List<List<int>> matrix1, List<List<int>> matrix2, int size);
    }
}
