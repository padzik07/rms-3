﻿using InterfaceRepository.MoveMaker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.GraphHandler
{
    public class GraphHandler : IGraphHandler
    {
        public bool CheckTwoMatrises(List<List<int>> matrix1, List<List<int>> matrix2, int size)
        {
            if (CheckEdgeCount(matrix1,matrix2) == false)
            {
                return false;
            }

            var rowPermutationList = CreatePermutationList(matrix2, 0,size - 1);
            foreach (var rowPermutation in rowPermutationList)
            {

            }
            return true;
        }

        public List<List<int>> ConmertToMatrix(string graph,int size)
        {
            var matrix = new List<List<int>>();
            for (int i = 0; i < size; i++)
            {
                matrix.Add(new List<int>());
                for (int j = 0; j < size; j++)
                {
                    matrix[i].Add(0);
                }
            }

            var player = 1;

            var graveEdgeList = graph.Split(' ');

            foreach (var edge in graveEdgeList)
            {
                var x = int.Parse(edge.Split(';')[0]);
                var y = int.Parse(edge.Split(';')[1]);
                matrix[x][y] = player;
                matrix[y][x] = player;

                player = 3 - player;
            }

            return matrix;
        }

        private bool CheckEdgeCount(List<List<int>> matrix1, List<List<int>> matrix2)
        {
            var count1 = 0;
            var count2 = 0;

            foreach (var row in matrix1)
            {
                foreach (var item in row)
                {
                    if (item != 0)
                    {
                        count1++;
                    }
                }
            }
            
            foreach (var row in matrix2)
            {
                foreach (var item in row)
                {
                    if (item != 0)
                    {
                        count2++;
                    }
                }
            }

            return count1 == count2;
        }

        private void SwapTwo<T>(ref T a, ref T b)
        {
            var proxy = a;
            a = b;
            b = proxy;
        }

        private List<T> CreatePermutationList<T>(List<T> list,int k, int m)
        {
            var permutationList = new List<T>();

            int i;
            if (k == m)
            {
                for (i = 0; i <= m; i++)
                {
                }
                return permutationList;
            }
            else
                for (i = k; i <= m; i++)
                {
                    var refList = list.ToArray();
                    SwapTwo(ref refList[k], ref refList[i]);
                    list = refList.ToList();
                    CreatePermutationList(list, k + 1, m);
                    SwapTwo(ref refList[k], ref refList[i]);
                    list = refList.ToList();
                }
            return permutationList;
        }

        private bool CheckSame(List<List<int>> matrixA, List<List<int>> matrixB)
        {
            var size = matrixA.Count;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (matrixA[i][j] != matrixB[i][j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
