﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.FileReader
{
    public class FileReader: IFileReader
    {
        private StreamReader _streamReader;

        public FileReader()
        {
        }

        public void OpenStream(string file)
        {
            _streamReader = new StreamReader(file);
        }

        public bool IsEnd
            => _streamReader != null ? _streamReader.EndOfStream : true;

        public string Read
            => _streamReader != null ? _streamReader.ReadLine() : "";

        public void Close()
        {
            if (_streamReader != null)
            {
                _streamReader.Close();
            }
        }
    }
}
