﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.FileReader
{
    public interface IFileReader
    {
        void OpenStream(string file);
        bool IsEnd { get; }
        string Read { get; }
        void Close();
    }
}
