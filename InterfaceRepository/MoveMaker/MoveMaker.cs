﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceRepository.MoveMaker
{
    public class MoveMaker : IMoveMaker
    {
        public List<string> MakeMoveList(int size)
        {
            List<string> moveList = new List<string>();
            for (int i = 0; i < size; i++)
            {
                for (int j = i + 1; j < size; j++)
                {
                    moveList.Add($"{i};{j}");
                }
            }
            return moveList;
        }
    }
}
