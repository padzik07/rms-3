﻿using Autofac;
using Program.Exectution.Main;
using Program.Exectution.Registration;
using System;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>().AsSelf();
            var conteiner = RegistrationModule.Register(builder);
            using (var scope = conteiner.BeginLifetimeScope())
            {
                scope.Resolve<Application>();
            }
            Console.ReadKey();
        }
    }
}
