﻿
using Autofac;
using InterfaceRepository.Exectution.Registration;
using ProgramApp.Exectution.Main;
using System;

namespace EntryProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder= new ContainerBuilder();
            var conteiner = MainRegister.Register(builder);
            using (var scope = conteiner.BeginLifetimeScope())
            {
                scope.Resolve<Application>();
            }
            Console.ReadKey();
        }
    }
}
