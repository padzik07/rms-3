﻿using Autofac;
using ProgramApp.Exectution.Main;

namespace EntryProgram
{
    public class MainRegister
    {

        public static IContainer Register(ContainerBuilder builder)
        {
            builder.RegisterType<Application>()
                .AsSelf();

            builder
                .RegisterModule(new InterfaceRepository.Exectution.Registration.RegistrationModule());

            builder
                .RegisterModule(new ProgramApp.RegistrationModule());

            builder
                .RegisterModule(new DataBaseControler.RegistrationModule());

            return builder.Build();
        }
    }
}
