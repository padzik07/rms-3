﻿using Common.Shared.DataObjects;
using DataBaseControler.CrudRepo;
using DataBaseControler.DTO;
using InterfaceRepository.Handlers;
using InterfaceRepository.RMSMain;
using ProgramApp.GameConfiguration.Presenter;
using System;

namespace ProgramApp.Exectution.Main
{
    public class Application
    {
        private readonly ITreeCheck _treeCheck;
        private readonly IGameStepHandler _gameStepHandler;
        private readonly GameConfigPresenter _gameConfigPresenter;

        public Application(
            ITreeCheck treeCheck,
            GameConfigPresenter gameConfigPresenter,
            IGameStepHandler gameStepHandler)
        {
            _treeCheck = treeCheck;
            _gameConfigPresenter = gameConfigPresenter;
            _gameStepHandler = gameStepHandler;
            Run();
        }

        public void Run()
        {
            _gameConfigPresenter.View.Run();
        }
    }
}
