﻿namespace ProgramApp.GameConfiguration.View
{
    partial class GameConfigView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSize = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelGrafSize = new System.Windows.Forms.Label();
            this.labelStartPoint = new System.Windows.Forms.Label();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.checkBoxPrediction = new System.Windows.Forms.CheckBox();
            this.textBoxMessageBox = new System.Windows.Forms.TextBox();
            this.textBoxWinner = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxSize
            // 
            this.textBoxSize.Location = new System.Drawing.Point(132, 44);
            this.textBoxSize.Name = "textBoxSize";
            this.textBoxSize.Size = new System.Drawing.Size(100, 26);
            this.textBoxSize.TabIndex = 0;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(12, 114);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(90, 32);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelGrafSize
            // 
            this.labelGrafSize.AutoSize = true;
            this.labelGrafSize.Location = new System.Drawing.Point(13, 47);
            this.labelGrafSize.Name = "labelGrafSize";
            this.labelGrafSize.Size = new System.Drawing.Size(113, 20);
            this.labelGrafSize.TabIndex = 5;
            this.labelGrafSize.Text = "Wielkość grafu";
            // 
            // labelStartPoint
            // 
            this.labelStartPoint.AutoSize = true;
            this.labelStartPoint.Location = new System.Drawing.Point(13, 79);
            this.labelStartPoint.Name = "labelStartPoint";
            this.labelStartPoint.Size = new System.Drawing.Size(113, 20);
            this.labelStartPoint.TabIndex = 6;
            this.labelStartPoint.Text = "Punkt startowy";
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(132, 76);
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.Size = new System.Drawing.Size(100, 26);
            this.textBoxStart.TabIndex = 7;
            // 
            // checkBoxPrediction
            // 
            this.checkBoxPrediction.AutoSize = true;
            this.checkBoxPrediction.Location = new System.Drawing.Point(17, 13);
            this.checkBoxPrediction.Name = "checkBoxPrediction";
            this.checkBoxPrediction.Size = new System.Drawing.Size(98, 24);
            this.checkBoxPrediction.TabIndex = 8;
            this.checkBoxPrediction.Text = "Wygrywa";
            this.checkBoxPrediction.UseVisualStyleBackColor = true;
            // 
            // textBoxMessageBox
            // 
            this.textBoxMessageBox.Location = new System.Drawing.Point(238, 13);
            this.textBoxMessageBox.Multiline = true;
            this.textBoxMessageBox.Name = "textBoxMessageBox";
            this.textBoxMessageBox.Size = new System.Drawing.Size(375, 133);
            this.textBoxMessageBox.TabIndex = 9;
            // 
            // textBoxWinner
            // 
            this.textBoxWinner.Location = new System.Drawing.Point(132, 11);
            this.textBoxWinner.Name = "textBoxWinner";
            this.textBoxWinner.Size = new System.Drawing.Size(100, 26);
            this.textBoxWinner.TabIndex = 10;
            // 
            // GameConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 171);
            this.Controls.Add(this.textBoxWinner);
            this.Controls.Add(this.textBoxMessageBox);
            this.Controls.Add(this.checkBoxPrediction);
            this.Controls.Add(this.textBoxStart);
            this.Controls.Add(this.labelStartPoint);
            this.Controls.Add(this.labelGrafSize);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.textBoxSize);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GameConfigView";
            this.Text = "Konfiguracja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSize;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelGrafSize;
        private System.Windows.Forms.Label labelStartPoint;
        private System.Windows.Forms.TextBox textBoxStart;
        private System.Windows.Forms.CheckBox checkBoxPrediction;
        private System.Windows.Forms.TextBox textBoxMessageBox;
        private System.Windows.Forms.TextBox textBoxWinner;
    }
}