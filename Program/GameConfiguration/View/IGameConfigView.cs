﻿using Common.MVP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramApp.GameConfiguration.View
{
    public interface IGameConfigView : IView
    {
        event EventHandler OnStart;

        void ShowMessage(string message);

        bool Prediction { get; }
        string StartPath { get; }
        int Size { get; }
        int WinnerPrediction { get; }
    }
}
