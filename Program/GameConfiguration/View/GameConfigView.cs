﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramApp.GameConfiguration.View
{
    public partial class GameConfigView : Form, IGameConfigView
    {
        public event EventHandler OnTest;
        public event EventHandler OnRun;
        public event EventHandler OnStart;
        public event EventHandler<string> OnGrafSizeFilled;
        public event EventHandler<string> OnStartPointFilled;

        public GameConfigView()
        {
            InitializeComponent();
        }

        public void Run()
        {
            ShowDialog();
        }

        public void ShowMessage(string message)
        {
            textBoxMessageBox.Clear();
            textBoxMessageBox.Text = message;
        }

        public bool Prediction
            => checkBoxPrediction.Checked;

        public string StartPath => textBoxStart.Text;

        public int Size
        {
            get
            {
                int result;
                return int.TryParse(textBoxSize.Text, out result) ? result : 4;
            }
        }

        public int WinnerPrediction
        {
            get
            {
                int result;
                return int.TryParse(textBoxWinner.Text, out result) ? result : 1;
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
            => OnStart?.Invoke(sender, e);
    }
}
