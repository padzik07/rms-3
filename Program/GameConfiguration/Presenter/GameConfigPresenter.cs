﻿using Common.MVP;
using InterfaceRepository.RMSMain;
using ProgramApp.GameConfiguration.View;
using System;

namespace ProgramApp.GameConfiguration.Presenter
{
    public class GameConfigPresenter : PresenterBase<IGameConfigView>
    {
        public IGameConfigView View;
        private readonly ITreeCheck _treeCheck;

        public GameConfigPresenter(
            IGameConfigView view,
             ITreeCheck treeCheck)
        {
            View = view;
            _treeCheck = treeCheck;

            _treeCheck.OnSendMessage += OnSendMessage;
            View.OnStart += OnStart;
        }

        private void OnSendMessage(object sender, string e)
        {
            View.ShowMessage(e);
        }

        private void OnStart(object sender, EventArgs e)
        {
            _treeCheck.Execution(View.Size, View.StartPath);
            View.ShowMessage("Koniec pracy");
        }
    }
}
