﻿using Autofac;
using ProgramApp.GameConfiguration.Presenter;
using ProgramApp.GameConfiguration.View;
using System.Linq;

namespace ProgramApp
{
    public class RegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<GameConfigPresenter>().AsSelf();

            builder.RegisterType<GameConfigView>().AsImplementedInterfaces();
        }
    }
}
