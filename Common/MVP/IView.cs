﻿namespace Common.MVP
{
    public interface IView
    {
        void Run();
    }
}
