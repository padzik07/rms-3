﻿namespace Common.MVP
{
    public class PresenterBase<T> : IPresenter where T : IView
    {
        private T _view;
        public T View
        {
            get => _view;
            set => _view = value;
        }

        public void RunView()
        {
            View.Run();
        }
    }
}
