﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enum
{
    public enum TableNameEnum
    {
        [Description("game_step")]
        GameStep,

        [Description("possible_moves")]
        PossibleMoves
    }
}
