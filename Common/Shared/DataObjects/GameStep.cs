﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Shared.DataObjects
{
    public class GameStep
    {
        public long Id { get; set; }
        public string GameState { get; set; }
        public int? Player { get; set; }
        public int? Winner { get; set; }
        public string Info { get; set; }
        public List<GameStep> PossibleGameSteps { get; set; } = new List<GameStep>();
    }
}
