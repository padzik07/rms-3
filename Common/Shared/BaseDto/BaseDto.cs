﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Shared.BaseDto
{
    public class BaseDto
    {
        public virtual long Id { get; set; }
    }
}
